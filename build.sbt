name := "scala-example"

version := "1.0"

scalaVersion := "2.12.2"

val akkaVersion = "2.5.4"
val akkaHttpVersion = "10.0.10"
val scalaTestVersion = "3.0.4"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" % "scala-xml_2.12" % "1.0.6",
  "com.typesafe.akka" % "akka-actor_2.12" % "2.5.4"
)