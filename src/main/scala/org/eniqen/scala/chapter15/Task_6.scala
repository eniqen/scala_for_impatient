package org.eniqen.scala.chapter15

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  val worker = new Worker()

  new Thread(() => {
    Thread.sleep(3000)
    worker.flag = true
    println("Thread-1: Set worker flag to true")
  }).start()

  new Thread(() => {
    println("Thread-2: check worker flag = " + worker.flag)
    while(!worker.flag) {
      Thread.sleep(200)
      println("Thread-2: sleep zZz")
    }
    println("Thread-2: end work with worker flag = " + worker.flag)
  }).start()
}

class Worker(@volatile var flag: Boolean = false)