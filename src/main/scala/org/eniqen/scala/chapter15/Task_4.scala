package org.eniqen.scala.chapter15

import scala.annotation.varargs

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 {

  @varargs
  def sum(args: Int*): Int = {
    args.sum
  }

}
