package org.eniqen.scala.chapter15

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

}

@deprecated
class AnnotationCLass(@deprecated val name: String) {
  @deprecated val age: Int = 5

  @deprecated
  def this() = this("empty")

  @deprecated
  def getResult(): Unit = {
    print("hello")
  }
}
