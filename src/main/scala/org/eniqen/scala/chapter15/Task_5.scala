package org.eniqen.scala.chapter15

import java.net.URL

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 {

  @throws(classOf[Exception])
  def read(_url: URL): String = {
    Source.fromURL(_url).mkString
  }
}
