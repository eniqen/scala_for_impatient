package org.eniqen.scala.chapter15

import scala.annotation.tailrec

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  @tailrec def sum(seq: List[Int], accumulator: Int): Int = {
    seq match {
      case head :: tail => sum(tail, accumulator + head)
      case Nil => accumulator
    }
  }

  println(sum(List(1, 2, 3), 0))
}