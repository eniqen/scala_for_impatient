package org.eniqen.scala.chapter15

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {
  private val list = List("A", "B", "C")
  println(list.init ::: list.drop(1).init ::: list.drop(2).init)

  def L[A](xs: A*) = xs.toList

  println(L(L(1, 2), L(3)).map(_.mkString("#")).mkString(":"))
  println(L(L(1), L(2), L(3)).map(_.mkString("#")).mkString(":"))
}
