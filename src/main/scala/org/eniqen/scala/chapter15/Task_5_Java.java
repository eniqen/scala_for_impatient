package org.eniqen.scala.chapter15;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Task_5_Java {
	public static void main(String[] args) throws Exception {
		System.out.println(Task_5.read(Task_5_Java.class.getResource("/1.txt")));
	}
}
