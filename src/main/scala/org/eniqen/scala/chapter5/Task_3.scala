package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {

  class Time(private var hrs: Int, private var mnts: Int) {

    hrs = hrs % 24
    mnts = mnts % 60

    def minutes = mnts

    def hours = hrs

    def before(other: Time): Boolean = {
      this.hrs < other.hrs || hrs == other.hrs && this.mnts < other.mnts
    }
  }

  private val time = new Time(23, 50)
  private val time2 = new Time(22, 18)
  print(time2.before(time))

}

