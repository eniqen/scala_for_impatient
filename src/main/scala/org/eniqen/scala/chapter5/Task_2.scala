package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  class BankAccount {
    private var _balance = 0.0

    def balance = _balance

    def deposit(amount: Int) {
      _balance += amount
    }

    def withdraw(amount: Int) {
      if (_balance - amount >= 0)
        _balance -= amount
    }
  }

  private val account = new BankAccount()
  account.deposit(1000)
  account.withdraw(999)
  assert(account.balance == 1)
}


