package org.eniqen.scala.chapter5;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Task_9 {
}

class Car {

	private final String manufacturer;
	private final String modelName;
	private int modelYear = -1;
	private String regNum = "";

	public Car(String manufacturer, String modelName) {
		this.manufacturer = manufacturer;
		this.modelName = modelName;
	}

	public Car(String manufacturer, String modelName, int modelYear) {
		this(manufacturer, modelName);
		this.modelYear = modelYear;
	}

	public Car(String manufacturer, String modelName, int modelYear, String regNum) {
		this(manufacturer, modelName, modelYear);
		this.regNum = regNum;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getModelName() {
		return modelName;
	}

	public int getModelYear() {
		return modelYear;
	}

	public String getRegNum() {
		return regNum;
	}

	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}
}
