package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {

  class Employee(val _name: String = "John Q. Public",
                 val _salary: Double = 0.0) {
  }
}
