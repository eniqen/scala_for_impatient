package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  class Person(var age: Int) {
    age = scala.math.abs(age)
  }

  print(new Person(-1).age)
}
