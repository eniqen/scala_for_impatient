package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  class Time(private var hrs: Int, private var mnts: Int) {

    hrs = hrs % 24
    mnts = mnts % 60
    private val totalTime = hrs * 60 + mnts - 1

    def minutes = mnts

    def hours = hrs

    def before(other: Time): Boolean = {
      this.totalTime < other.totalTime
    }
  }

  private val time = new Time(23, 50)
  private val time2 = new Time(22, 18)
  print(time2.before(time))
}
