package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  class Car(val _manufacturer: String,
            val _modelName: String,
            val _modelYear: Int = -1,
            var regNum: String = "") {

    override def toString: String = _manufacturer + " " + _modelName + " " + _modelYear + " " + regNum
  }

  private val car = new Car("111", "222")
  private val car2 = new Car("333", "444", 3)
  private val car3 = new Car("555", "666", 4, "777")

  println(car.toString)
  println(car2.toString)
  println(car3.toString)
}
