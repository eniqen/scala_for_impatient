package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  class Person(private val fullName: String) {
    private val names = fullName.split(" ")
    def lastName = names(0)
    def firstName = names(1)
  }
  private val person = new Person("Петров Василий")

  println(person.lastName)
  println(person.firstName)
}
