package org.eniqen.scala.chapter5

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  class Counter {
    private var value = Integer.MAX_VALUE - 3

    def increment() {
      if (value < Integer.MAX_VALUE) value += 1
    }

    def current = value
  }

  private val counter = new Counter()

  println(counter.current)
  counter.increment()
  counter.increment()
  counter.increment()
  counter.increment()
  counter.increment()
  counter.increment()
  println(counter.current)
}
