package org.eniqen.scala.chapter5

import scala.beans.BeanProperty

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  class Student(@BeanProperty var id: Long,
                @BeanProperty var name: String)

  private val student = new Student(1, "Петя")

  student.setId(5)
  student.setName("Вася")

  println(student.getId, student.getName)
}
