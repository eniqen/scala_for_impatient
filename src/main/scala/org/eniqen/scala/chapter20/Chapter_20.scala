package org.eniqen.scala.chapter20

import akka.actor.PoisonPill

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Chapter_20 extends App {
  import akka.actor.{Actor, ActorSystem, Props}

  class HiActor extends Actor {
    override def receive: Receive = {
      case Charge(ccnum, merchant, amt) => println(ccnum + " " + merchant + " "  + amt)
      case "Hi" => println("Hello World")
      case _ => unhandled()
    }
  }

  object HiActor {
    def props = Props(new HiActor())
  }

  val system = ActorSystem.create("test")
  val helloActor = system.actorOf(HiActor.props)

  helloActor ! "Hi"
  helloActor.tell("Hi", Actor.noSender)
  helloActor.tell("123", Actor.noSender)
//  helloActor ! PoisonPill

  case class Charge(creditCardNumber: Long, merchant: String, amount: Double)

  helloActor ! Charge(41111L, "Fred's Bail and Tackle", 19.95)
}
