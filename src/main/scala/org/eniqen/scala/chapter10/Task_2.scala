package org.eniqen.scala.chapter10


/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  import java.awt.Point

  class OrderPoint(x: Int, y: Int) extends Point(x, y) with Ordered[Point] {

    override def compare(that: Point): Int = {
      if (this.x < that.x || this.x == that.x && this.y < that.y) -1 else 1
    }
  }

  val orderPoint1 = new OrderPoint(10, 5)
  val orderPoint2 = new OrderPoint(10, 6)

  val orderPoint5 = new OrderPoint(4, 6)
  val orderPoint6 = new OrderPoint(10, 6)

  val orderPoint7 = new OrderPoint(11, 6)
  val orderPoint8 = new OrderPoint(10, 6)

  assert(orderPoint1 < orderPoint2)
  assert(orderPoint5 < orderPoint6)
  assert(orderPoint7 > orderPoint8)
}
