package org.eniqen.scala.chapter10

import java.io.{BufferedInputStream, File, FileInputStream, InputStream}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  trait Logger {
    def log(message: String): Unit = {}
  }

  trait ConsoleLogger extends Logger {
    override def log(message: String): Unit = {
      println(message)
    }
  }

  trait Buff extends Logger {
    this: FileInputStream =>
    val buff = new BufferedInputStream(this)

    override def read(b: Array[Byte]): Int = {
      log("Current " + buff.available())
      buff.read(b)
    }
  }

  val fis = new FileInputStream(new File(getClass.getResource("/chapter10_4.txt").getPath)) with Buff with ConsoleLogger
  private val buffer = new Array[Byte](256)
  private val result = Iterator.continually(fis.read(buffer)).takeWhile(_ != -1).map(_.toString).mkString
  println(result)
}
