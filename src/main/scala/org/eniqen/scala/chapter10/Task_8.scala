package org.eniqen.scala.chapter10

import java.io.{BufferedInputStream, File, FileInputStream, InputStream}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  trait BufferedIs extends InputStream {
    this: FileInputStream =>
    val buf = new BufferedInputStream(this)

    override def read(b: Array[Byte]): Int = buf.read(b)

  }

  val fis = new FileInputStream(new File(getClass.getResource("/chapter10_4.txt").getPath)) with BufferedIs
  fis.read(new Array[Byte](512))
}
