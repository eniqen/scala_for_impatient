package org.eniqen.scala.chapter10

import java.io.{File, PrintStream}
import java.time.LocalDate
import java.util.Date

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Chapter10 extends App {

  trait Logger {
    def log(message: String) {}

    def info(message: String) {
      log("INFO: " + message)
    }

    def warn(message: String) {
      log("WARN: " + message)
    }

    def severe(message: String) {
      log("SEVERE: " + message)
    }
  }

  trait ConsoleLogger extends Logger {
    override def log(message: String): Unit = {
      println(message)
      super.log(message)
    }
  }

  trait TimestampLogger extends Logger {
    override def log(message: String): Unit = super.log(LocalDate.now() + " " + message)
  }

  trait ShortLogger extends Logger {
    val maxLength: Byte

    override def log(message: String): Unit = {
      super.log(if (message.length < maxLength) message
                else message.substring(0, maxLength)
      )
    }
  }

  trait FileLogger extends Logger {
    var filename: String
    val out = new PrintStream(new File(filename).getPath)
    out.println("# " + new Date().toString)

    override def log(message: String): Unit = {
      out.println(message)
      out.flush()
    }
  }

  trait LoggerExeption extends Logger {
    this: Exception =>
    override def log(message: String): Unit = super.log(getMessage)

    // this: { def getMessage(): String } =>
    // override def log(message: String): Unit = super.log(getMessage())
  }

  class UnhappyExeption extends Exception with LoggerExeption {
    override def getMessage: String = "Привет"
  }

  abstract class Account {
    var balance: Double = 0.0

    def withdraw(amount: Double)
  }

  class SavingAccount extends Account with Logger {
    def withdraw(amount: Double): Unit = {
      if (amount > balance) severe("Недостаточный баланс")
      else balance -= amount
    }
  }

  val savingAccount = new SavingAccount with ConsoleLogger
  savingAccount.withdraw(5)

  val savingAccount2 = new SavingAccount with Logger
  savingAccount2.withdraw(5)

  val savingAccount3 = new SavingAccount with ConsoleLogger with TimestampLogger
  savingAccount3.withdraw(5)

  val savingAccount4 = new SavingAccount with ConsoleLogger with TimestampLogger with ShortLogger {
    val maxLength: Byte = 6
  }
  savingAccount4.withdraw(5)

//  val savingAccount5 = new {
//    override val filename = "test.txt"
//  } with SavingAccount with FileLogger
}
