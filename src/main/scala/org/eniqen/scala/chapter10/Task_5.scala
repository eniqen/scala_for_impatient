package org.eniqen.scala.chapter10

import java.beans.{PropertyChangeEvent, PropertyChangeListener}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  trait PropertyChangeSupport {

    import java.beans.{PropertyChangeSupport => JProp}

    val propertyChangeSupport = new JProp(this)

    def addPropertyChangeListener(listener: PropertyChangeListener) {
      propertyChangeSupport.addPropertyChangeListener(listener)
    }

    def removePropertyChangeListener(listener: PropertyChangeListener) {
      propertyChangeSupport.removePropertyChangeListener(listener)
    }

    def getPropertyChangeListeners: Array[PropertyChangeListener] = {
      propertyChangeSupport.getPropertyChangeListeners()
    }

    def addPropertyChangeListener(propertyName: String, listener: PropertyChangeListener) {
      propertyChangeSupport.addPropertyChangeListener(propertyName,listener)
    }


    def removePropertyChangeListener(propertyName: String, listener: PropertyChangeListener) {
      propertyChangeSupport.removePropertyChangeListener(propertyName, listener)
    }

    def getPropertyChangeListeners(propertyName: String): Array[PropertyChangeListener] = {
      propertyChangeSupport.getPropertyChangeListeners(propertyName)
    }


    def firePropertyChange(propertyName: String, oldValue: Any, newValue: Any): Unit = {
      propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue)
    }

    def firePropertyChange(propertyName: String, oldValue: Int, newValue: Int): Unit = {
      propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue)
    }

    def firePropertyChange(propertyName: String, oldValue: Boolean, newValue: Boolean): Unit = {
      propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue)
    }

    def firePropertyChange(event: PropertyChangeEvent) {
      propertyChangeSupport.firePropertyChange(event)
    }
  }

}
