package org.eniqen.scala.chapter10

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  trait Logger {
    def log(message: String) {}
  }

  class CryptoLogger extends Logger {
    var key: Byte = 3

    override def log(message: String): Unit = {
      println(message.map(_.toInt + key).map(_.toChar).mkString)
    }
  }

  val cryptoLogger = new CryptoLogger
  cryptoLogger.log("Hello")
}
