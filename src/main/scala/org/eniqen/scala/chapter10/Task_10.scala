package org.eniqen.scala.chapter10

import java.io.{File, FileInputStream, InputStream}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {

  class IterableInputStream(val is: InputStream) extends InputStream with Iterable[Byte] {

    override def read(): Int = is.read()

    override def iterator: Iterator[Byte] = new Iterator[Byte] {
      def hasNext: Boolean = is.available() > 0

      def next(): Byte = is.read().toByte
    }
  }

  val fis = new IterableInputStream(new FileInputStream(new File(getClass.getResource("/chapter10_4.txt").getPath)))
  for (byte <- fis) print(byte.toChar)
}
