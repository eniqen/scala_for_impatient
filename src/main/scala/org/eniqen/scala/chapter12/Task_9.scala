package org.eniqen.scala.chapter12

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {
  val stringArray = Array("Hello", "World")
  val numArray = Array(5, 6)

 def correspondents(first: Array[String], second: Array[Int], fun: (String, Int) => Boolean): Boolean = {
   first.zip(second).map(elem => fun(elem._1, elem._2)).reduce(_ & _)
 }

  println(correspondents(stringArray, numArray, (x, y) => x.length == y))
}
