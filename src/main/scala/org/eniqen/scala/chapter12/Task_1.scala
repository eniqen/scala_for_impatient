package org.eniqen.scala.chapter12

import scala.collection.immutable.IndexedSeq

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {
  def values(fun: (Int) => Int, low: Int, high: Int): IndexedSeq[(Int, Int)] = {
    for(value <- low to high) yield (value, fun(value))
  }

  println(values(x => x * x, -5, 5))
}
