package org.eniqen.scala.chapter12

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  def factorial(value: Int): Int = if (value < 1) 1 else (1 to value).reduceLeft(_ * _)

  println(factorial(5))
}

