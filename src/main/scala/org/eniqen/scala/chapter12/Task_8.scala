package org.eniqen.scala.chapter12

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {
  val stringArray = Array("Hello", "World")
  val numArray = Array(5, 6)

  println(stringArray.corresponds(numArray)(_.length == _))

}
