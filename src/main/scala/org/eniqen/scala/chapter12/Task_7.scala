package org.eniqen.scala.chapter12

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {
  def adjustToPair(func: (Int, Int) => Int)(pair: (Int, Int)): Int = func(pair._1, pair._2)

  val pairs = (1 to 10).zip(11 to 20)

  println(pairs.map(adjustToPair(_ * _)))
  println(adjustToPair((x, y) => x * y)(6, 7))
}
