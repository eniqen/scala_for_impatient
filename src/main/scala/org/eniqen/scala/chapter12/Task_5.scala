package org.eniqen.scala.chapter12

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  def largest(fun: (Int) => Int, inputs: Seq[Int]): Int = {
    inputs.map(fun).reduceLeft((left, right) => if (left > right) left else right)
  }

  println(largest(x => 10 * x - x * x, 1 to 10))
}
