package org.eniqen.scala.chapter12

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {
  def max(array: Array[Int]): Int = {
    array.reduceLeft((left, right) => if (left > right) left else right)
  }

  println(max(Array(999, 66 , 1, -5, 2344, 334)))
}
