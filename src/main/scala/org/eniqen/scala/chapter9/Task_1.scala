package org.eniqen.scala.chapter9

import java.io.PrintWriter

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  val url = getClass.getResource("/chapter10_1.txt")
  val source = Source.fromFile(url.getPath)
  val lines = source.getLines.toArray.reverse
  val writer = new PrintWriter(url.getPath)

  writer.write(lines.mkString("\n"))

  writer.close()
  source.close()
}
