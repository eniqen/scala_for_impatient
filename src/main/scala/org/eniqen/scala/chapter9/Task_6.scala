package org.eniqen.scala.chapter9

import org.eniqen.scala.chapter9.Task_7.getClass

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  private val line = Source.fromFile(getClass.getResource("/chapter10_4.txt").getPath).mkString
  private val res = """"(.+?)"""".r

  for(r <- res.findAllIn(line)) println(r)
}
