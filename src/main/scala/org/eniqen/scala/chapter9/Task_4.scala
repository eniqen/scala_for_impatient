package org.eniqen.scala.chapter9

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  private val line = Source.fromFile(getClass.getResource("/chapter10_4.txt").getPath)
  private val result = "\\d*\\.*\\d+".r.findAllIn(line.mkString).map(_.toDouble).toArray

  println(result.max)
  println(result.min)
  println(result.sum / result.length)
  println(result.sum)
}
