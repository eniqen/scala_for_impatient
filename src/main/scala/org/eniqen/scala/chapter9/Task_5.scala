package org.eniqen.scala.chapter9

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  for (index <- 0 to 20) println("%7.0f  %f".format(math.pow(2, index), math.pow(2, -index)))
}
