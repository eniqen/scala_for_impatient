package org.eniqen.scala.chapter9

import java.io.File

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  def subDirs(file: File): Int = {
    val childs = file.listFiles()
    childs.count(_.getName.endsWith(".scala")) + childs.filter(_.isDirectory)
                                                       .map(subDirs)
                                                       .sum
  }

  val dirs = subDirs(new File("src/main/scala"))
  println(dirs)
}
