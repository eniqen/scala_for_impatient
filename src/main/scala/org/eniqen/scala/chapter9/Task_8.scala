package org.eniqen.scala.chapter9

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  private val regex = """<img([^>]+)src="(.+?)"""".r
  private val html = Source.fromURL("https://habrahabr.ru/post/188010/").mkString

  for (regex(a, b) <- regex.findAllIn(html)) println(b)
}
