package org.eniqen.scala.chapter9

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {

  @SerialVersionUID(1) class Person(val name: String) extends Serializable {
    val friends = new ArrayBuffer[Person]()
  }

  val person1 = new Person("Вася")
  val person2 = new Person("Петя")
  val person3 = new Person("Слава")
  val person4 = new Person("Костя")

  person1.friends += person2
  person1.friends += person3
  person3.friends += person4

  val array = Array[Person](person1, person2, person3, person4)

  val out = new ObjectOutputStream(new FileOutputStream("1.tmp"))
  out.writeObject(array)

  val in = new ObjectInputStream(new FileInputStream("1.tmp"))
  val readPersons = in.readObject().asInstanceOf[Array[Person]]

  assert(readPersons.length == readPersons.length)
}
