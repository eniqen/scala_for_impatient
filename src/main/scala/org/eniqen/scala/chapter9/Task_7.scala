package org.eniqen.scala.chapter9

import org.eniqen.scala.chapter9.Task_4.getClass

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  private val line = Source.fromFile(getClass.getResource("/chapter10_4.txt").getPath).mkString
  private val result =  "\\d*\\.*\\d+".r.replaceAllIn(line, "")

  for(res <- result) print(res)
}
