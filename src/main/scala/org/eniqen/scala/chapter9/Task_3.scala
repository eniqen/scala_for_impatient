package org.eniqen.scala.chapter9

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  Source.fromFile(getClass.getResource("/1.txt").getPath)
    .mkString
    .split("\\W+")
    .distinct
    .filter(_.length > 12)
    .foreach(println(_))
}

