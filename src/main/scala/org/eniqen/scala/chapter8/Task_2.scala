package org.eniqen.scala.chapter8

import java.time.LocalDate

import org.eniqen.scala.chapter8.Task_1.BankAccount

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {
  val account = new SavingAccount(100)
  account.withdraw(1)
  account.withdraw(1)
  account.withdraw(1)
  account.deposit(1)
  account.deposit(1)
  account.deposit(1)

  assert(account.currentBalance() == 97)
}

class SavingAccount(var initBalance: Double,
                    val freeTransaction: Int = 3,
                    val rate: Double = 0.1) extends BankAccount(initBalance) {

  private var month = LocalDate.now()
  private var transactionPerMount = 0
  private val commission = 1

  def earnMontlyInterest() = {
    if (month.plusMonths(1).isBefore(LocalDate.now())) {
      transactionPerMount = 0
      month = LocalDate.now()
      super.deposit(balance * rate)
    }
  }

  def isFree() = {
    transactionPerMount <= freeTransaction
  }

  override def withdraw(amount: Double): Unit = {
    earnMontlyInterest()
    transactionPerMount += 1
    super.withdraw(amount + (if (isFree()) 0 else commission))
  }

  override def deposit(amount: Double): Unit = {
    earnMontlyInterest()
    transactionPerMount += 1
    super.deposit(amount - (if (isFree()) 0 else commission))
  }
}
