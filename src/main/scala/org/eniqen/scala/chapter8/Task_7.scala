package org.eniqen.scala.chapter8

import java.awt.Rectangle

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  class Square(x: Int,
               y: Int,
               width: Int) extends Rectangle(x, y, width, width) {

    def this() {
      this(0, 0, 0)
    }

    def this(width: Int) {
      this(0, 0, width)
    }
  }

  val s1 = new Square()
  val s2 = new Square(150)
  val s3 = new Square(2, 3, 150)
}
