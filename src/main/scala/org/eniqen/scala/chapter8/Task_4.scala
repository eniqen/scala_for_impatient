package org.eniqen.scala.chapter8

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  abstract class Item {
    def price: Double
    def description: String
  }

  class SimpleItem(val price: Double, val description: String) extends Item {}

  class Bundle extends Item {
    val items = new ArrayBuffer[Item]()

    override def price: Double = items.map(_.price).sum

    override def description: String = items.map(_.description).mkString(", ")
  }

  val item1 = new SimpleItem(23.9, "Item-1")
  val item2 = new SimpleItem(11.9, "Item-2")

  val bundle = new Bundle()
  bundle.items += item1
  bundle.items += item2

  println(bundle.price)
  println(bundle.description)

}
