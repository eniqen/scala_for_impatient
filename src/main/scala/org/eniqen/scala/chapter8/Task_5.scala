package org.eniqen.scala.chapter8

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  class Point(val x: Int, val y: Int) {}

  class LabelPoint(val label: String, override val x: Int, override val y: Int) extends Point(x, y)

  val labelPoint = new LabelPoint("point-1", 5, 10)
  println(labelPoint.x)
  println(labelPoint.y)
  println(labelPoint.label)
}
