package org.eniqen.scala.chapter8

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  class BankAccount(initBalance: Double) {
    protected var balance = initBalance

    def withdraw(amount: Double) = {
      balance -= amount
    }

    def deposit(amount: Double) = {
      balance += amount
    }

    def currentBalance() = this.balance
  }

  class ChekingAccount(initBalance: Double) extends BankAccount(initBalance) {

    override def withdraw(amount: Double) = {
      getCommission()
      super.withdraw(amount)
    }

    override def deposit(amount: Double) = {
      getCommission()
      super.deposit(amount)
    }

    private def getCommission() = balance -= 1
  }

  val account = new ChekingAccount(1000)
  account.deposit(100)

  assert(account.currentBalance() == 1099)
}
