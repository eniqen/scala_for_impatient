package org.eniqen.scala.chapter8

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  class Point(val x: Double, val y: Double)

  abstract class Shape {
    def centerPoint(): Point
  }

  class Rectangle(x1: Int, x2: Int, y1: Int, y2: Int) extends Shape {
    override def centerPoint() = new Point(x1 * x2 / 2, y1 * y2 / 2)
  }

  class Circle(r: Int) extends Shape {
    private val point = math.Pi * math.pow(r, 2)

    override def centerPoint() = new Point(point, point)
  }

  val c = new Circle(3)
  println(c.centerPoint().x)
  println(c.centerPoint().y)
}
