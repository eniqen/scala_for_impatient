package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  class ASCIIArt(val art: String) {
    def +(other: ASCIIArt): ASCIIArt = {
      val zip = this.art.replace("\r", "").split("\n")
        .zip(other.art.replace("\r", "").split("\n"))
      new ASCIIArt(zip.map(x => x._1 + x._2).mkString("\n"))
    }

    def |(other: ASCIIArt) = new ASCIIArt(this.art + "\n" + other.art)
  }

  val cat =
    """
 /\_/\
( * * )
(  -  )
 | | |
(__|__)"""

  val say =
    """
    -----
  / Hello \
<  Scala  |
  \ Coder /
   -----"""

  val aSCIIArt1 = new ASCIIArt(cat)
  val aSCIIArt2 = new ASCIIArt(say)

  println((aSCIIArt1 + aSCIIArt2).art)
  println((aSCIIArt1 | aSCIIArt2).art)
}
