package org.eniqen.scala.chapter11

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  class Table {
    val tags = new ArrayBuffer[String]()

    def |(text: String): Table = {
      tags += "<td>%s</td>".format(text)
      this
    }

    def ||(text: String): Table = {
      tags += "</tr><tr>\n<td>%s</td>".format(text)
      this
    }

    def build(): String = {
      "<table><tr>\n%s</tr></table>".format(tags.mkString)
    }
  }

  object Table {
    def apply(): Table = new Table()
  }

  println((Table() | "Java" | "Scala" || "Gosling" | "Odersky" || "JVM" | "JVM, .NET").build())

}
