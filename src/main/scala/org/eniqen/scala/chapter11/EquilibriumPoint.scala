package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
class EquilibriumPoint extends App {

  def solution(array: Array[Int]): Int = {

    var totalSum: Long = array.map(_.toLong).sum
    var leftSum: Long = 0

    for (index <- array.indices) {
      totalSum -= array(index)
      if (leftSum == totalSum) return index
      leftSum += array(index)
    }
    -1
  }

  println(solution(Array(1082132608, 0, 1082132608)))
}
