package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  class BitSequence {
    private var value: Long = _

    def apply(s: String): Long = java.lang.Long.parseLong(s, 2)

    def update(index: Int, newBit: Int): Unit = {
      value |= (newBit & 1L) << index % 64
    }

    override def toString: String = value.toString
  }

  val x = new BitSequence()
  println(x)
  println(x("010101010101110111010101010101010101010101110101010101010101111"))
  x(50) = 1
  println(x)
}
