package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  object RichFile {

    def unapply(path: String): Option[(String, String, String)] = {
      if (path.nonEmpty) {
        val pointIndex: Int = path.lastIndexOf(".")
        val slashIndex: Int = path.lastIndexOf("/")

        val fileName: String = {if (pointIndex > -1 && slashIndex > -1) path.substring(slashIndex + 1, pointIndex) else ""}
        val extension: String = {if (pointIndex > -1) path.substring(pointIndex + 1, path.length) else ""}
        val filePath: String = {if (slashIndex > -1) path.substring(0, slashIndex + 1) else ""}

        Some(filePath, fileName, extension)
      }  else {
        None
      }
    }
  }

  val RichFile(path, name, extension) = "C:/temp/document.txt"
  assert( path == "C:/temp/" )
  assert( name == "document" )
  assert( extension == "txt" )
}
