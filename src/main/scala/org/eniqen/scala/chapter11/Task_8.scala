package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  class Matrix(val rowsSize: Int = 2, val colsSize: Int = 2) {

    private val matrix: Array[Array[Int]] = Array.ofDim[Int](rowsSize, colsSize)

    def apply(r: Int, c: Int): Int = matrix(r)(c)

    def update(r: Int, c: Int, v: Int) {
      matrix(r)(c) = v
    }

    def +(other: Matrix): Matrix = {
      if (this.dim != other.dim) throw new IllegalArgumentException("Incorrect rows columns size")
      val result = new Matrix(rowsSize, colsSize)
      for {
        row <- 0 until rowsSize
        col <- 0 until colsSize
      } result.update(row, col, this.apply(row, col) + other.apply(row, col))
      result
    }

    def *(other: Matrix): Matrix = {
      if (this.dim != other.dim) throw new IllegalArgumentException("Incorrect rows columns size")
      val result = new Matrix(rowsSize, colsSize)
      for {
        row <- 0 until rowsSize
        col <- 0 until colsSize
      } result(row, col) = this (row, col) * other(row, col)
      result
    }

    def *(other: Int): Matrix = {
      val result = new Matrix(rowsSize, colsSize)
      for {
        row <- 0 until rowsSize
        col <- 0 until colsSize
      } result(row, col) = this (row, col) * other
      result
    }

    private def dim: (Int, Int) = (rowsSize, colsSize)

    override def toString: String = matrix.map(_.mkString(" ")).mkString("\n")
  }

  val matrix = new Matrix()
  matrix + new Matrix()
  println(matrix.toString)
}
