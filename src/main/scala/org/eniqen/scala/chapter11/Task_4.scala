package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  class Money(_value: Int) extends Ordered[Money] {

    private val currency = _value

    def +(that: Money): Money = {
      new Money(currency + that.currency)
    }

    def -(that: Money): Money = {
      new Money(currency - that.currency)
    }

    def *(that: Money): Money = {
      new Money(currency * that.currency)
    }

    def /(that: Money): Money = {
      new Money(currency / that.currency)
    }

    def ==(that: Money): Boolean = {
      this.currency == that.currency
    }

    override def compare(that: Money): Int = {
      this.currency - that.currency
    }
  }

  override def hashCode(): Int = super.hashCode()

  override def equals(obj: scala.Any): Boolean = super.equals(obj)

  object Money {
    def apply(_dollars: Int, _cents: Byte): Money = new Money(_dollars * 100 + _cents)
  }

  val money1 = Money(0, 50)
  val money2 = Money(1, 75)
  val money3 = money1 + money2

  assert(money1 < money2)
  assert(money3 == Money(2, 25))
}
