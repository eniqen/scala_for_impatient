package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {

  object RichFile {
    def unapplySeq(path: String): Option[Seq[String]] = if (path.nonEmpty) Some(path.split("/")) else None
  }


  val RichFile(path, name, extension) = "C:/temp/document.txt"

  assert(path == "C:")
  assert(name == "temp")
  assert(extension == "document.txt")
}
