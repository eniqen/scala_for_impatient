package org.eniqen.scala.chapter11

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {

  class Fraction(_n: Int, _d: Int) {

    private val num: Int = if (_d == 0) 1 else _n * sign(_d) / gcd(_n, _d)
    private val den: Int = if (_d == 0) 0 else _d * sign(_d) / gcd(_n, _d)

    override def toString = num + "/" + den

    def sign(a: Int): Int = if (a > 0) 1 else if (a < 0) -1 else 0

    def gcd(a: Int, b: Int): Int = if (b == 0) math.abs(a) else gcd(b, a % b)

    def +(that: Fraction): Fraction = {
      Fraction(num * that.den + that.num * den, den * that.den)
    }

    def -(that: Fraction): Fraction = {
      Fraction(num / that.den - that.num / den, den / that.den)
    }

    def *(that: Fraction): Fraction = Fraction(this.num * that.num, this.den * that.den)

    def /(that: Fraction): Fraction = Fraction(this.num / that.den, this.den / that.num)
  }

  object Fraction {
    def apply(_n: Int, _d: Int): Fraction = new Fraction(_n, _d)
  }

  val a = new Fraction(1, 3)
  val b = new Fraction(1, 2)
  println(a + b)
}

