package org.eniqen.scala.chapter4

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  val map = Map("Шорты" -> 30.0, "Футболка" -> 18.3, "Кросовки" -> 42.1)
  val discountMap = for ((k, v) <- map) yield (k, v * 0.9)
  print(discountMap)
}
