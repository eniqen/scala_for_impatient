package org.eniqen.scala.chapter4

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  val url = getClass.getResource("/1.txt")
  val words = Source.fromURL(url).mkString.split("\\W+")
  val wordCount = (
    for (word <- words.distinct)
      yield (word, words.count(_ == word))
    ).toMap
  println(wordCount)
}

