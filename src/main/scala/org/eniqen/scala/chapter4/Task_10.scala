package org.eniqen.scala.chapter4

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {
  print(  "Hello".zip("World"))
}
