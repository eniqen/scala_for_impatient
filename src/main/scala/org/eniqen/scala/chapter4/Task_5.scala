package org.eniqen.scala.chapter4

import scala.collection.JavaConverters
import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  val treeMap = JavaConverters.mapAsScalaMap(new java.util.TreeMap[String, Int])
  val url = getClass.getResource("/1.txt")
  val words = Source.fromURL(url).mkString.split("\\W+")
  for (word <- words)
    treeMap += (word -> (treeMap.getOrElse(word, 0) + 1))

  print(treeMap)
}
