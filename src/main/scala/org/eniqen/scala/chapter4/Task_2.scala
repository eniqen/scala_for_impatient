package org.eniqen.scala.chapter4

import scala.io.Source._

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  val map = new scala.collection.mutable.HashMap[String, Int]()
  val iterator = fromInputStream(getClass.getResourceAsStream("/1.txt"))
    .getLines()
    .map(_.split("\\W+"))
    .flatMap(_.toIterable)
  while (iterator.hasNext) {
    var word = iterator.next()
    map += (word -> (map.getOrElse(word, 0) + 1))
  }
  println(map)
}
