package org.eniqen.scala.chapter4

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {
  def minmax(array: Array[Int]): (Int, Int) = {
    (array.min, array.max)
  }
}
