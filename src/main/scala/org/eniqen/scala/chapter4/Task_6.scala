package org.eniqen.scala.chapter4

import java.util.Calendar

import scala.collection.mutable

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  val linkedHashMap = mutable.LinkedHashMap[String, Int]("Monday" -> Calendar.MONDAY,
                                                         "Tuesday" -> Calendar.THURSDAY,
                                                         "Wednesday" -> Calendar.WEDNESDAY,
                                                         "Thursday" -> Calendar.THURSDAY,
                                                         "Friday" -> Calendar.FRIDAY,
                                                         "Saturday" -> Calendar.SATURDAY,
                                                         "Sunday" -> Calendar.SUNDAY)

  for (day <- linkedHashMap) print(day)
}
