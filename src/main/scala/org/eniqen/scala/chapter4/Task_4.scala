package org.eniqen.scala.chapter4

import scala.io.Source

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  var sortedMap = scala.collection.immutable.SortedMap[String, Int]()
  val url = getClass.getResource("/1.txt")
  val words = Source.fromURL(url).mkString.split("\\W+")

  for (word <- words)
    sortedMap += (word -> (sortedMap.getOrElse(word, 0) + 1))
  print(sortedMap)
}
