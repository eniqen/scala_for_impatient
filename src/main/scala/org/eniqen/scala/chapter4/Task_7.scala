package org.eniqen.scala.chapter4

import scala.collection.JavaConverters

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  val properties: scala.collection.Map[String, String] = {
    JavaConverters.propertiesAsScalaMap(System.getProperties)
  }
  val maxLength = properties.keys.maxBy(_.length).length
  for ((k, v) <- properties) printf("%-" + maxLength + "s |%s\n", k, v)
  for ((k, v) <- properties) println(k.padTo(maxLength, ' ') + "| " + v)
}
