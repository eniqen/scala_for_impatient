package org.eniqen.scala.chapter4

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {
  def lteggt(array: Array[Int], v: Int): (Int, Int, Int) = {
    (array.count(_ > v), array.count(_ == v), array.count(_ < v))
  }
}
