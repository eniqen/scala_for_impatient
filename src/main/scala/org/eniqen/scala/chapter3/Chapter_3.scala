package org.eniqen.scala.chapter3

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Chapter_3 extends App {
  val nums = new Array[Int](5)
  val a = new Array[String](10)
  val s = Array("Hello", "World")
  s(0) = "GoodBye"
  println(s)

  val b = ArrayBuffer[Int]()
  b += 1
  b += (1, 3, 4, 5, 6)
  b ++= Array(9, 13, 21)
  println(b)
  b.trimEnd(4)
  println(b)
  b.insert(4, 100)
  println(b)
  b.remove(4)
  println(b)
  b.remove(1, 2)
  println(b)

  b.toArray
  a.toBuffer

  for (i <- b.indices) print(i + " ")
  println()
  for (i <- 0 until(b.length, 2)) print(i + " ")

  println()
  for (i <- b.indices.reverse) print(i + " ")

  println()
  val arr = Array(1, 2, 3, 4, 5)
  val result = for (i <- arr) yield i * 2
  println(result.toBuffer)
  scala.util.Sorting.quickSort(result)
  println()

  val result2 = for (i <- result if i > 4) yield i * i
  println(result2.toBuffer)
  println(result2.sum)
}
