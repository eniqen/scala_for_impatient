package org.eniqen.scala.chapter3

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  val array: Array[Int] = Array(1, 1, 2, 3, 3, 6, 4, 7, 3, 8, 3, 2).distinct
  print(array.mkString)
}
