package org.eniqen.scala.chapter3

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {
  val array = Array(1, 2, 3, 4, 5).sortWith(_ > _)
  val arrayBuffer = ArrayBuffer(1, 2, 3, 4, 5).sortWith(_ > _)

  println(array.mkString(","))
  println(arrayBuffer.mkString(","))
}
