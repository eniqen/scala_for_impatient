package chapter3

import scala.util.Random

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  private def fillArray(n: Int) = {
    val array = new Array[Int](n)
    for (i <- array.indices) array(i) += i
    array
  }

  println(fillArray(5).toBuffer)


  def test(n: Int): Array[Int] = {
    new Array[Int](5).map(_ => Random.nextInt(n))
  }

  println(test(5).toBuffer)
}
