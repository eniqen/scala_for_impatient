package org.eniqen.scala.chapter3

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {
  var a = Array(2, 3, 1, -11, 33, 5, -7, 11, -44)
  var result = for (elem <- a) yield elem * 2

  a.filter(_ % 2 == 0).map(_ * 2)

  var first = true
  var n = a.length
  var i = 0

  while (i < n) {
    if (a(i) >= 0) i += 1
    else {
      if (first) {
        first = false
        i += 1
      } else {
        a.drop(i)
        n -= 1
      }
    }
  }

  println(a.mkString(","))

  var a2 = ArrayBuffer(2, 3, 1, -11, 33, 5, -7, 11, -44)
  var indexes = for (elem <- a2.indices if a2(elem) < 0) yield elem
  indexes = indexes.reverse
  indexes = indexes.dropRight(1)

  for (elem <- indexes.indices) a2.remove(indexes(elem))

  println(a2.mkString(","))
}
