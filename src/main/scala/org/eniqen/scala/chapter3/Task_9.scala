package org.eniqen.scala.chapter3

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  val array = java.util.TimeZone.getAvailableIDs()
    .filter(_.startsWith("America/"))
    .map(_.stripPrefix("America/"))

  print(array.mkString(",\n"))
}
