package chapter3

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  def swap(array: Array[Int]): Array[Int] = {
    for (elem <- array.indices if elem % 2 == 0) {
      var temp = array(elem)
      if (elem != array.length - 1) {
        array.update(elem, array(elem + 1))
        array.update(elem + 1, temp)
      }
    }
    array
  }

  val actual = Array[Int](1, 2, 3, 4, 5)
  val expected = Array[Int](2, 1, 4, 3, 5)

  assert(swap(actual).deep == expected.deep)
  assert(swap(actual) sameElements  actual)
}
