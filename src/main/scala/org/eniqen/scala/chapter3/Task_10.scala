package org.eniqen.scala.chapter3

import scala.collection.JavaConverters

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {
  import java.awt.datatransfer._
  val flavours = SystemFlavorMap.getDefaultFlavorMap.asInstanceOf[SystemFlavorMap]
  val flavor = JavaConverters.asScalaBufferConverter(flavours.getNativesForFlavor(DataFlavor.imageFlavor)).asScala
  print(flavor)
}
