package org.eniqen.scala.chapter3

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {
  def swap(array: Array[Int]): Array[Int] = array.sortWith(_ > _)

  val array = Array(1, 5, 9, 3, 5, 6, 7)
  println(swap(array).mkString(","))

  val array2 = Array(1, 5, 9, -3, 5, 6, -7)
  val result = ArrayBuffer[Int]()
  result ++= (for (i <- array2.indices if array2(i) > 0) yield array2(i))
  println(result)

  val r = result.partition(_ < 0)
  r._1 ++ r._2
  println(r)
}
