package chapter3

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {

  def swap(array: Array[Int]): Array[Int] = (
    for (index <- array.indices)
      yield if (index % 2 == 0) {
        if (index < array.length - 1)
          array(index + 1)
        else array(index)
      } else array(index - 1)
    ).toArray

  val actual1 = Array[Int](1, 2, 3, 4, 5)
  val expected1 = Array[Int](2, 1, 4, 3, 5)

  val actual2 = Array[Int](1, 2, 3, 4, 5, 6)
  val expected2 = Array[Int](2, 1, 4, 3, 6, 5)

  assert(swap(actual1).deep == expected1.deep)
  assert(!(swap(actual1) sameElements actual1))

  assert(swap(actual2).deep == expected2.deep)
  assert(!(swap(actual2) sameElements actual2))
}

