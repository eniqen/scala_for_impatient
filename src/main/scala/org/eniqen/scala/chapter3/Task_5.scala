package org.eniqen.scala.chapter3

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {
  def avg(array: Array[Double]): Double = {
    array.sum / array.length
  }

  val array = Array(2.4, 2.5, 8.4, 1.0)
  print(avg(array))
}
