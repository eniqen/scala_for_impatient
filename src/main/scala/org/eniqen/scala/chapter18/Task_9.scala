package org.eniqen.scala.chapter18

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  abstract class Dim[T](val value: Double, val name: String) {
    this: T =>
    protected def create(v: Double): T
    def +(other: Dim[T]): T = create(value + other.value)

    override def toString: String = value + " " + name
  }

  class Seconds(v: Double) extends Dim[Seconds](v, "s") {
    override protected def create(v: Double): Seconds = new Seconds(v)
  }

  class Meters(v: Double) extends Dim[Meters](v, "m") {
    override protected def create(v: Double): Meters = new Meters(v)
  }

  val seconds1 = new Seconds(4)
  val seconds2 = new Seconds(1)

  val meters1 = new Meters(5)
  val meters2 = new Meters(5)

  println(seconds1 + seconds2)
  println(meters1 + meters2)
}
