package org.eniqen.scala.chapter18

import org.eniqen.scala.chapter18.SIDE.SIDE

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  class Bug {
    private var index: Int = 0
    private var side: SIDE = SIDE.RIGHT

    def move(stepCount: Int): this.type = {
      if (this.side eq SIDE.RIGHT) this.index += stepCount else this.index -= stepCount
      this
    }

    def turn(): this.type = {
      if (this.side eq SIDE.RIGHT) side = SIDE.LEFT else side = SIDE.RIGHT
      this
    }

    def show(): this.type = {
      print(index + " ")
      this
    }
  }

  new Bug().move(4).show().move(6).show().turn().move(5).show()

}

object SIDE extends Enumeration {
  type SIDE = Value
  val LEFT = Value
  val RIGHT = Value
}
