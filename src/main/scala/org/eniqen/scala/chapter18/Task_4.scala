package org.eniqen.scala.chapter18

import scala.collection.mutable.ArrayBuffer

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  val twitter = new Network
  val vk = new Network

  val vasya1 = twitter.join("Вася")
  val vasya2 = vk.join("Вася")
  val vasya3 = vk.join("Вася")

  println(vasya2.equals(vasya3))
  println(vasya3)
}

class Network {
  private val members = new ArrayBuffer[Member]

  class Member(name: String) {
    private val outerRef = Network.this
    val contacts = new ArrayBuffer[Network#Member]()

    def equals(that: Member): Boolean = {
      this.outerRef == that.asInstanceOf[Member].outerRef
    }
  }

  def join(name: String): Member = {
    val m = new Member(name)
    members += m
    m
  }
}