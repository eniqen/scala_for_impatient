package org.eniqen.scala.chapter18

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  def printValues(f: Int => Int, from: Int, to: Int) = for (x <- from to `to`) print(f(x) + " ")

  printValues((x: Int) => x * x, 3, 6)
  println()
  printValues(Array(1, 1, 2, 3, 5, 8, 13, 21, 34, 55), 3, 6)
}
