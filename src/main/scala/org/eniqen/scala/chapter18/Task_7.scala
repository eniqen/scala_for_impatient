package org.eniqen.scala.chapter18

import java.io.{BufferedReader, FileReader}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  trait Close[T <: {def close() : Unit}] {
    def test(obj: T): Unit = {
      try {
        println(123)
      } finally {
        obj.close()
      }
    }
  }

  trait T1 {
    this: AutoCloseable =>

    def test[T <: this.type](obj: T): Unit = {
      try {

      } finally {
        obj.close()
      }
    }
  }

  val reader = new BufferedReader(new FileReader(this.getClass.getResource("/1.txt").getFile)) with T1
//  reader.test(reader)
}
