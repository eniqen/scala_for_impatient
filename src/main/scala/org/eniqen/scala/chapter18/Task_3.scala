package org.eniqen.scala.chapter18

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {

  trait Item
  object Title extends Item
  object Author extends Item

  class Document {
    private var next: Item = _
    private var author: String = _
    private var title: String = _

    def set(item: Item): this.type = {
      this.next = item
      this
    }

    def to(text: String): this.type = {
      next match {
        case Title => title = text
        case Author => author = text
      }
      this
    }

    override def toString: String = this.title + " " + this.author
  }

  val book = new Document
  print(book set Title to "Scala for the Impatient" set Author to "Cay")
}
