package org.eniqen.scala.chapter18

import org.eniqen.scala.chapter18.Task_1.Bug

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  object Then
  object Around
  object Show

  val show = Show
  val around = Around
  val `then` = Then

  trait FluentBug {
    this: Task_1.Bug =>
    def and(obj: Show.type): this.type = {this.show()}
    def and(obj: Then.type): this.type = this
    def turn(obj: Around.type): this.type = {this.turn()}
  }

  val bugsy = new Task_1.Bug with FluentBug
  bugsy move 4 and `then` move 6 and show turn around move 5 and show
}


