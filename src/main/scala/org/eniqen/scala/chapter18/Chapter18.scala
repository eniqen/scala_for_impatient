//package org.eniqen.scala.chapter18
//
//import java.awt.Rectangle
//import java.awt.event.ActionEvent
//import java.awt.image.BufferedImage
//import java.io.File
//import javax.imageio.ImageIO
//
//import scala.collection.mutable
//import scala.collection.mutable.ArrayBuffer
//import scala.io.Source
//
///**
//* @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
//  */
//object Chapter18 {
//  val document = new Document().setAuthor("Pushkin").setTitle("Title-1")
//  val book = new Book().setTitle("Title-2").addChapter("")
//
//  val chatter = new Network
//  val myFace = new Network
//
//  val fred = chatter.join("Fred")
//  val wilma = chatter.join("Wilma")
//  val barney = myFace.join("Barney")
//  fred.contacts += barney // суем барни который в друппе фейса в контакты фреда хотя тот с другой сети через проекцию
//
//  def appendLines(target: {def append(str: String): Any},
//                  lines: Iterable[String]) {
//    for (l <- lines) {
//      target.append(l)
//      target.append("\n")
//    }
//  }
//
//  val image = new ArrayBuffer[java.awt.Shape with Serializable]()
//  val rect = new Rectangle(5, 10, 20, 30)
//
//  def foo(x: Array[T] forSome {type T <: CharSequence}) = x.foreach(y => println(y.length))
//
//  //  image += rect
//  //  image += new Area(rect)
//
//  def process[M <: n.Member forSome {val n : Network}](m1: M, m2: M) = (m1, m2)
//
//  process(fred, wilma)
//  process(fred, barney)
//
//}
//
//class Document {
//  private var title: String = _
//  private var author: String = _
//  private var useNextArgAs: Any = _
//
//  def setTitle(title: String): this.type = {
//    this.title = title
//    this
//  }
//
//  def setAuthor(author: String): this.type = {
//    this.author = author
//    this
//  }
//
//  def set(obj: Title.type): this.type = {
//    this.useNextArgAs = obj
//    this
//  }
//
//  def to(arg: String) = if (useNextArgAs == Title) title = arg else ""
//}
//
//class Book extends Document {
//  var chapter: String = _
//
//  def addChapter(chapter: String) = {
//    this.chapter = chapter
//    this
//  }
//}
//
//object Title
//
//class Network {
//
//  private val members = new ArrayBuffer[Member]
//
//  class Member(val name: String) {
//    val contacts = new ArrayBuffer[Network#Member]
//  }
//
//  def join(name: String): Member = {
//    val m = new Member(name)
//    members += m
//    m
//  }
//}
//
//class Book2 {
//
//  import scala.collection.mutable._
//
//  type Index = HashMap[String, (Int, Int)]
//}
//
//abstract class Reader {
//  type Contents
//
//  def read(fileName: String): Contents
//}
//
//trait Logger {
//  def log(msg: String)
//}
//
//class FileLogger(file: String) extends Logger {
//  override def log(msg: String): Unit = ???
//}
//
//class MockAuth(file: String) extends Auth with Logger {
//  override def login(id: String, password: String): Boolean = ???
//
//  override def log(msg: String): Unit = ???
//}
//
//trait LoggedException extends Logger {
//  this: Exception =>
//  def log() {
//    log(getMessage)
//  }
//}
//
//trait Auth {
//  this: Logger =>
//  def login(id: String, password: String): Boolean
//}
//
//trait App {
//  this: Logger with Auth =>
//}
//
//object MyApp extends App with FileLogger
//
//("test.log") with MockAuth ("users.txt")
//
//
//trait Group {
//  outer: Network =>
//
//  class Member {
//  }
//}
//
//
//abstract trait LoggerComponent {
//
//  trait Logger {}
//
//  val logger: Logger
//
//  class FileLogger(file: String) extends Logger {}
//}
//
//
//abstract  trait AuthComponent {
//  this: LoggerComponent =>
//
//  trait Auth {}
//
//  val auth: Auth
//
//  class MockAuth(file: String) extends Auth {}
//}
//
//object AppComponent extends LoggerComponent with AuthComponent {
//  val logger = new FileLogger("123")
//  val auth = new MockAuth("users.txt")
//}
//
//trait Reader {
//  type Content
//  def read(fileName: String): Content
//}
//
//trait Reader[T] {
//  def read(fileName: String): T
//}
//
//class StringReader extends Reader {
//  type Contents = String
//  override def read(fileName: String): String = Source.fromFile(fileName, "UTF-8").mkString
//}
//
//class ImageReader extends Reader {
//  type Content = BufferedImage
//  override def read(fileName: String): BufferedImage = ImageIO.read(new File(fileName))
//}
//
////trait Listener[E] {
////  def occurred(e: E): Unit
////}
////
////trait Source[E, L <: Listener[E]] {
////  private val listeners = new ArrayBuffer[L]
////  def add(l: L) { listeners += l }
////  def remove(l: L) { listeners +=l }
////  def fire(e: E): Unit = {
////    for (l <- listeners) l.occurred(e)
////  }
////}
////
////trait ActionListener extends Listener[ActionEvent]
////
////class Button extends Source[ActionEvent, ActionListener] {
////  def click() {
////    fire(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "click"))
////  }
////}
////
////trait Event[S] {
////  var source: S = _
////}
////
////trait Listener2[S, E <: Event[S]] {
////  def occurred(e: E): Unit
////}
////
////trait Source2[S, E <: Event[S], L <: Listener2[S, E]] {
////  this: S =>
////  private val listeners = new ArrayBuffer[L]
////  def add(l: L) { listeners +=l }
////  def remove(l: L) { listeners -=l }
////  def fire(e: E): Unit = {
////    e.source = this
////    for(l <- listeners) l.occurred(e)
////  }
////}
////
////class ButtonEvent extends Event[Button]
////
////trait ButtonListener extends  Listener2[Button, ButtonEvent]
////
////class Button extends Source2[Button, ButtonEvent, ButtonListener] {
////  def click() { fire(new ButtonEvent) }
////}
//
//trait ListenerSupport {
//  type S <: Source
//  type E <: Event
//  type L <: Listener
//
//  trait Event {
//    var source: S = _
//  }
//
//  trait Listener {
//    def occurred(e: E): Unit
//  }
//
//  trait Source {
//    this: S=>
//    private val listeners = new ArrayBuffer[L]
//    def add(l: L) { listeners += l }
//    def remove(l: L) { listeners -= l }
//    def fire(e: E) {
//      e.source = this
//      for(l <- listeners) l.occurred(e)
//    }
//  }
//}
//
//object ButtonModule extends ListenerSupport {
//  type S = Button
//  type E = ButtonEvent
//  type L = ButtonListener
//
//  class ButtonEvent extends Event
//  trait ButtonListener extends Listener
//  class Button extends Source
//
//  def click () { fire(new ButtonEvent) }
//}
//
//trait Iterable[E] {
//  def iterator(): Iterator[E]
//  def map[F](f: (E) => F): mutable.Buffer[F]
//}