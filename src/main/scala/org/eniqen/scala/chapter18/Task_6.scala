package org.eniqen.scala.chapter18

import scala.collection.SortedSet

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  def find(array: SortedSet[Int], value: Int): Int Either Int = {
    array.zipWithIndex
      .collectFirst {
        case (`value`, i) => Right(i)
        case (v, i) if v > value => Right(i)
      }.getOrElse(Left(array.size - 1))
  }

  val result = find(SortedSet(1, 2, 3, 4, 5, 6, 7, 10), 8)

  println(result.left + " " + result.right)
}
