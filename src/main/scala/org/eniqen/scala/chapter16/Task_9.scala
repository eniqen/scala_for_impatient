package org.eniqen.scala.chapter16

import java.io.File

import scala.io.Source
import scala.xml._
import scala.xml.dtd.{DocType, PublicID}
import scala.xml.parsing.XhtmlParser
import scala.xml.transform.{RewriteRule, RuleTransformer}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  val path = getClass.getResource("/index.xhtml").getPath
  val file = new File(path)
  val source = Source.fromFile(file)
  val parser = new XhtmlParser(source)
  val document = parser.initialize.document()


  val rule = new RewriteRule {
    override def transform(n: Node): Seq[Node] = n match {
      case item @ <img/> if item.attribute("alt").isEmpty => item.asInstanceOf[Elem] % Attribute(null, "alt", "TODO", Null)
      case _ => n
    }
  }

  val transformedResult = new RuleTransformer(rule).transform(document)

  println(transformedResult)

  XML.save("newFile.html", transformedResult.head,
           enc = "UTF-8",
           xmlDecl = true,
           doctype = DocType("html", PublicID("-//W3C//DTD XHTML 1.0 Strict//EN",
                                              "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"), Nil)
  )}
