package org.eniqen.scala.chapter16

import scala.collection.mutable.ArrayBuffer
import scala.xml.{Elem, Text}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  def toMap(elem: Elem): Map[String, String] = {
    val dt, dd = new ArrayBuffer[String]()
    elem.child.foreach({
      case <dt>{Text(t)}</dt> => dt += t
      case <dd>{Text(t)}</dd> => dd += t
    })
    dt.zip(dd).toMap
  }

  def parse(elem: Elem): Map[String, String] = {
    val types = elem.child.groupBy(_.label)
    types("dd").zip(types("dt")).map(x => (x._1.text, x._2.text)).toMap
  }

  val xml = <dl><dt>A</dt><dd>1</dd><dt>B</dt><dd>2</dd></dl>

  println(toMap(xml))
  println(parse(xml))
}
