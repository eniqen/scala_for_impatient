package org.eniqen.scala.chapter16

import scala.xml._

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Chapter_16 extends App {

//  val doc = <html><head><title>Fred's Memoirs</title></head></html>

  val items = <li>Fred</li><li>Wilma</li>
  val elem = <a href="http://scala-lang.org">The <em>Scala</em> language</a>

  for(n <- elem.child) println(n)

  val foo = <foo><bar type="greet">hi</bar><bar type="count">1</bar><bar type="color">yellow</bar></foo>

  val items2 = new NodeBuffer
  items2 += <li>Fred</li>
  items2 += <li>Wilma</li>
  val nodes : NodeSeq = items2

  val elem2 = <a href="http://scala-lang.org">The Scala language</a>
  val url = elem2.attributes("href")

  val image = <img alt="San Jose State University logo" src="http://www.sjsu.edu/publicaffairs/pics/sjsu_logo_color_web.jpg"/>
  val alt = image.attributes("alt")

  val url2 = elem2.attributes("href").text

  val optionalUrl = elem2.attributes.get("href").getOrElse(Text(""))


  for(attr <- elem2.attributes) println(attr.key + " " + attr.value)


  val image3 = <img alt="TODO" src="hamster.jpg"/>
  val map = image3.attributes.asAttrMap

  <ul><li>{items(0)}</li><li>{items(1)}</li></ul>


  <ul>{for (i <- items2) yield <li>{i}</li>}</ul>
  <a id={new Atom(1)}/>

  <img alt={if ("123" == "TODO") None else Some(Text("123"))}/>

  val js = <script><![CDATA[if temp < 0 alert("Cold!")]]></script>

  val code = """if (temp < 0 alert("Cold!"))"""
  val js2 = <script>{PCData(code)}</script>

  val g1 = <xml:group><li>Item 1</li><li>Item 2</li></xml:group>
  val g2 = Group(Seq(<li>Item 1</li>, <li>Item 2</li>))

  val items3 = <li>Item 1</li><li>Item 2</li>
  for (n <- <xml:group>{items3}</xml:group>) yield n

  for (n <- <ol>{items3}</ol>) yield n


  val list = <dl><dt>Java</dt><dd>Gosling</dd><dt>Scala</dt><dd>Odersky</dd></dl>
  val language = list \ "dt"
  println(language)

  println((<img src="hamster.jpg"/><img src="frog.jpg"/> \\ "@src").text)

  val textMatching = <li>An <em>important</em> item</li>

  textMatching match {
    case <li>{child}</li> => print("!!!" + child.text)
    case _ =>
  }

  textMatching match {
    case <li>{Text(text)}</li> => print("!_!" + text)
    case _ =>
  }

  textMatching match {
    case <li>{item @ _*}</li> => for (i <- item ) yield i
    case _ =>
  }
}
