package org.eniqen.scala.chapter16

import java.io.File

import scala.io.Source
import scala.xml.parsing.XhtmlParser


/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  val path = getClass.getResource("/index.xhtml").getPath
  val file = new File(path)
  val source = Source.fromFile(file)
  val parser = new XhtmlParser(source)
  val document = parser.initialize.document()

  (document \\ "a")
    .filter(img => img.attribute("alt").isEmpty)
    .foreach(println(_))
}
