package org.eniqen.scala.chapter16

import scala.xml.Elem

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  def convertXml(values: Map[String, String]): Elem = {
    <dl>{for((key, value) <- values) yield <dt>{key}</dt><dd>{value}</dd>}</dl>
  }
  println(convertXml(Map("A" -> "1", "B" -> "2")))
}
