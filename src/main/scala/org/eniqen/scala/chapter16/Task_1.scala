package org.eniqen.scala.chapter16

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {
  val x = <freq>Test1</freq><freq>Test2</freq>(0)
  val y = <freq>Test1<em>Lol1</em><em>Lol2</em></freq><freq>Test2</freq>(0)(0)
  println(x)
  println(y)
}
