package org.eniqen.scala.chapter16

import scala.xml.{Atom, Attribute, Text}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  val x = <li>Fred</li> match { case <li>{Text(t)}</li> => t }
  println(x)

  val y = <li>{"Fred"}</li> match { case <li>{t:Atom[_]}</li> => t }
  println(y)


}
