package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {
  def product(s: String): BigInt = {
    if (s.isEmpty) 1
    else s.head * product(s.tail)
  }
  println(product("Hello"))
}
