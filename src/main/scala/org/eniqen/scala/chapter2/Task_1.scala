package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  def signum(arg: Int): Int = {
    if (arg > 0) 1
    else if (arg < 0) -1
    else 0
  }

  assert(signum(4) == 1)
  assert(signum(0) == 0)
  assert(signum(-4) == -1)
}
