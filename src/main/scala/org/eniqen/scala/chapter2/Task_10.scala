package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {

  def pow(x: BigDecimal, n: Int): BigDecimal = {
    if (n == 0) 1
    else if (n < 0) 1 / pow(x, -n)
    else if (n % 2 == 0) {
      val y = pow(x, n / 2)
      y * y
    }
    else x * pow(x, n - 1)
  }

  println(pow(2, 2))
}
