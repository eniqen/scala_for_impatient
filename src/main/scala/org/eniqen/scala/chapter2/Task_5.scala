package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  def countdown(n: Int): Unit = {
    for (index <- n to (0, -1)) println(index)
  }
  countdown(5)
}
