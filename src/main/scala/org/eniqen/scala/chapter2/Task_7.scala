package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {
  println("Hello".foldLeft(1: BigInt)((a, b) => a * b))
}
