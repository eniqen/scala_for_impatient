package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {
  private def product(s: String) = s.foldLeft(1: BigInt)((a, b) => a * b)
  println(product("Hello"))

}
