package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {
  def multy(line: String): BigInt = {
    var result: BigInt = 1
    for (i <- line) result *= math.BigInt.int2bigInt(i.toInt)
    result
  }

  println(multy("Hello"))
}
