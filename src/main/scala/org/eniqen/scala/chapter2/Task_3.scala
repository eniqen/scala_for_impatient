package org.eniqen.scala.chapter2

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  var y: Int = 0
  val x: Unit = y = 1
  print(x)
}
