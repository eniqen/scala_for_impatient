package org.eniqen.scala.chapter1

import scala.util.Random

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Chapter_1 {
  def main(args: Array[String]): Unit = {
    println("crazy" * 3)
    println(BigInt(2).pow(1024))
    println(BigInt.probablePrime(100, Random).toString(36))

    val s = "line"
    println(s.head)
    println(s.last)

    val x = 5
    val testIf: Any = if (x > 0) "result" else -1
    val testIfUnit = if (x > 0) 1 else ()
    println(testIf)
    println(testIfUnit)

    if (x > 0) 1
    else if (x == 0) 0 else -1

    for (i <- 1 to 10) print(i)
    println()
    for (i <- 1 until 10) print(i)
    println()
    for (char <- "Привет") println(char)

    for (x <- 1 to 10; y <- "Tst") println(x + " " + y)

    lazy val words = scala.io.Source.fromFile("/test").mkString

    var variable: Int = 0
    val variable2: Unit = variable = 1
    println(variable2)
  }


  def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }

  def factorial(n: Int): Int = {
    def loop(n: Int, acc: Int): Int = {
      if (n == 0) acc else loop(n - 1, acc * n)
    }

    loop(n, 1)
  }

  private def summ(args: Int*) = {
    var result = 0
    for (index <- args) result += index
    result
  }

  private def recursiveSumm(args: Int*): Int = {
    if (args.isEmpty) 0
    else args.head + recursiveSumm(args.tail: _*)
  }

  private def signum(arg: Int) = {
    val result = Math.signum(arg)
    if (result > 0) result
    else if (result < 0) -result
    else 0f
  }
}

