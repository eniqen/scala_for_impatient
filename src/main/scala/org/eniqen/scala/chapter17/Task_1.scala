package org.eniqen.scala.chapter17

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {
  val pair = new Pair[String, Int]("Test", 1)
  val swapPair = pair.swap()

  assert(pair.first == swapPair.second)
  assert(pair.second == swapPair.first)

  sealed class Pair[T, S](val first: T, val second: S) {
    def swap() = new Pair(second, first)
  }
}

