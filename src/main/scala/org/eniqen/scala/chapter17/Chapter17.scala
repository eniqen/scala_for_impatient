package org.eniqen.scala.chapter17

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Chapter17 extends App {
  val pair1 = new Pair(42, "String")
  val pair2 = new Pair[Any, Any](42, "String")

  def getMiddle[T](a: Array[T]): T = a(a.length / 2)

  val f = getMiddle[String] _
  val str = f.apply(Array("123", "111", "321"))
  println(str)

  val pair3 = new ComparablePair[String]("Test1", "Test2")
  println(pair3.smaller)

  //  val pair4 = new ComparablePair[Int](1, 2) not work
  //  println(pair4.smaller)

  def makePair[T: Manifest](first: T, second: T) = {
    val r = new Array[T](2)
    r(0) = first
    r(1) = second
    r
  }

  println(makePair(1, 2).mkString(", "))
  private val person = new PersonPair[Student](Student(1, "", 1), Student(2, "", 1))

  val t = new Test()
  t.makeFriends(person)
  val student = new Student2()
  val person2 = new Person2()

  t.makeFriendWith(student, person2)

}

class ComparablePair[T <: Comparable[T]](val first: T, val second: T) {

  def smaller: T = if (first.compareTo(second) < 0) first else second

  def replaceFirst(newFirst: T) = new ComparablePair[T](newFirst, second)
}

class ImplicitPair[T: Ordering](val first: T, val second: T) {
  def smaller(implicit ord: Ordering[T]) =
    if (ord.compare(first, second) < 0) first else second
}

class ImplicitPair2[T](val first: T, val second: T)(implicit ev: T <:< Comparable[T])

abstract class Person(age: Int, name: String)

case class Student(age: Int, name: String, group: Int) extends Person(age, name);

class PersonPair[+T](val first: T, val second: T)

class Test() {
  def makeFriends(pair: PersonPair[Person]): Unit = {
    println("do somethink")
  }

  def makeFriendWith(s: Student2, f: Friend[Student2]) = f.befrend(s)
}

trait Friend[-T] {
  def befrend(someone: T)
}

class Person2 extends Friend[Person2] {
  override def befrend(someone: Person2): Unit = ???


}

class Student2 extends Person2 {
  def friends(students: Array[Student2], find: (Student2) => Person2) = {
    for (s <- students) yield find(s)
  }

  def findStudent(p: Person2): Student2 = {
    new Student2
  }

  friends(Array(new Student2(), new Student2()), findStudent)
}

class Test2[+T](val x: T, val y: T) {
  def replaceFirst[R >: T](newFirst: R) = new Test2[R](newFirst, x)
}