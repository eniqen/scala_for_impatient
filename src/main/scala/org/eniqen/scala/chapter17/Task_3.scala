package org.eniqen.scala.chapter17

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  class Pair[T, S](val first: T, val second: S) {}

  def swap[T, S](p: Pair[T, S]): Pair[S, T] = {
    new Pair(p.second, p.first)
  }
}
