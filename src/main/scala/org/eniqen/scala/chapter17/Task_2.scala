package org.eniqen.scala.chapter17

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {
  class Pair[T](var first: T, var second: T) {
    def swap() = {
      val temp = first
      first = second
      second = temp
    }
  }

  val pair = new Pair[Int](1, 2)
  pair.swap()

  assert(pair.first == 2)
  assert(pair.second == 1)
}

