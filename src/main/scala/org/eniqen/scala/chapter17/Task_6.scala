package org.eniqen.scala.chapter17

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {
  def middle[A](seq: Iterable[A]) : A = seq.toIndexedSeq(seq.size / 2)
}
