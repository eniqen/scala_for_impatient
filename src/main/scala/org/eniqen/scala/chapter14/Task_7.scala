package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  sealed abstract class BinaryTree

  case class Leaf(value: Int) extends BinaryTree

  case class Node(items: BinaryTree*) extends BinaryTree

  def leafSum(tree: BinaryTree): Int = tree match {
    case Leaf(leaf) => leaf
    case Node(items@_*) => items.map(leafSum).sum
    case _ => 0
  }

  val tree = Node(Node(Leaf(3), Leaf(8)), Leaf(2), Node(Leaf(5)))

  println(leafSum(tree))
}
