package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {
  def getNotEmptySum(items: List[Option[Int]]): Int = items.flatten.sum

  val items = List(Some(5), None, Some(16), Some(1))

  println(getNotEmptySum(items))
}
