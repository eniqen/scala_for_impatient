package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {
  def swap(pair: (Int, Int)): (Int, Int) = pair match {
    case (f, t) => (t, f)
  }

  val pair = (1, 2)

  val expected = (2, 1)

  assert(swap(pair) == expected)
}
