package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_10 extends App {

  type T = Double => Option[Double]

  def compose(left: T, right: T): T = {
    (x: Double) =>
      left(x) match {
        case Some(value) => right(value)
        case _ => None
      }
  }

  def f(x: Double): Option[Double] = if (x >= 0) Some(math.sqrt(x)) else None

  def g(x: Double): Option[Double] = if (x != 1) Some(1 / (x - 1)) else None

  val h: T = compose(f, g)
}

