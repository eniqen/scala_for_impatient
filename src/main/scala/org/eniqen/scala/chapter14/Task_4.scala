package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  sealed abstract class Item

  case class Product(description: String, price: Double) extends Item

  case class Bundle(description: String, discount: Double, items: Item*) extends Item

  case class Multiple(count: Int, item: Item) extends Item

  def price(item: Item): Double = item match {
    case Product(_, price) => price
    case Bundle(_, discount, items@_*) => items.map(price).sum - discount
    case Multiple(count, it) => price(it) * count
    case _ => 0
  }

  val bundle = Multiple(10,
    Bundle("Сумка-1", 5.0,
      Product("Продукт-1", 99.0),
      Product("Продукт-2", 340.0))
  )

  val x = Option(5)
  val myData = x match {
    case Some(data) => data
    case _ => 0
  }

  println(myData)


//  println(price(bundle))
}
