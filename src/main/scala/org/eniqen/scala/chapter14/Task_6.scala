package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  sealed abstract class BinaryTree

  case class Leaf(value: Int) extends BinaryTree

  case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

  def leafSum(tree: BinaryTree): Int = tree match {
    case Leaf(leaf) => leaf
    case Node(left, right) => leafSum(left) + leafSum(right)
    case _ => 0
  }

  val tree = Node(Node(Leaf(3), Leaf(5)), Node(Leaf(8), Node(Node(Leaf(1), Leaf(2)), Leaf(23))))

  print(leafSum(tree))
}
