package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  def leafSum(list: List[Any]): Int = {
    list.map {
      case list: List[Int] => leafSum(list)
      case value: Int => value
      case _ => 0
    }.sum
  }

  val list = List(List(3, 8), 2, List(5))

  println(leafSum(list))
}
