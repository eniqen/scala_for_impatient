package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {


  sealed abstract class BinaryTree

  case class Leaf(value: Int) extends BinaryTree

  case class Node(operator: Char, items: BinaryTree*) extends BinaryTree

  def eval(tree: BinaryTree): Int = tree match {
    case Leaf(leaf) => leaf
    case Node(operator, items@_*) => operator match {
      case '+' => items.map(eval).sum
      case '-' => -items.map(eval).sum
      case '*' => items.map(eval).product
    }
    case _ => 0
  }

  val tree = Node('+', Node('*', Leaf(3), Leaf(8)), Leaf(2), Node('-', Leaf(5)))

  println(eval(tree))
}
