package org.eniqen.scala.chapter14

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {

  def swap(array: Array[Int]): Array[Int] = array match {
    case Array(a, b, tail@_*) => Array(b, a) ++ tail
    case _ => array
  }

  val array = Array(1, 2, 3, 4, 5)
  val expected = Array(2, 1, 3, 4, 5)

  assert(swap(array) sameElements expected)
}
