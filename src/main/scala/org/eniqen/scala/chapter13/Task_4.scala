package org.eniqen.scala.chapter13

import scala.collection.Map

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  val arr = Array("Tom", "Fred", "Harry")
  val map = Map("Tom" -> 3, "Dick" -> 4, "Harry" -> 5)

  def matchValueWithForComprehension(array: Array[String], map: Map[String, Int]): Array[Int] = {
    for (elem <- array if map.contains(elem)) yield map(elem)
  }

  def matchValueWithFlatMap(array: Array[String], map: Map[String, Int]): List[Int] = array.flatMap(map.get).toList

  val expected = Array(3, 5)
  assert (expected sameElements matchValueWithFlatMap(arr, map))
  print(expected sameElements matchValueWithForComprehension(arr, map))
}