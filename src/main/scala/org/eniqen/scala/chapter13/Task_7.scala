package org.eniqen.scala.chapter13

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  val prices = List(5.0, 20.0, 9.95)
  val quantities = List(10, 2, 1)

  private val sum = prices.zip(quantities).map(Function.tupled(_ * _)).sum

  private val sum2 = (prices, quantities).zipped.map(_ * _).sum

  println(sum)
  println(sum2)
}
