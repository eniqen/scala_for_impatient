package org.eniqen.scala.chapter13

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  def grouped(array: Array[Double], columnCount: Int): Array[Array[Double]] = array.grouped(columnCount).toArray

  println(grouped(Array(1, 2, 3, 4, 5, 6), 3).deep.mkString("\n"))
}
