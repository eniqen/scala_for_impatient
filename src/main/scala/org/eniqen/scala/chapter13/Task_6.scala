package org.eniqen.scala.chapter13

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  val items = List(1, 2, 3, 4, 5)

  val foldLeft = (items :\ List[Int]()) (_ :: _)
  val foldRight = (List[Int]() /: items) (_ :+ _)
  val revert = (List[Int]() /: items) ((x, y) => y :: x)

  println(foldLeft)
  println(foldRight)
  println(revert)

}
