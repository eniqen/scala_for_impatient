package org.eniqen.scala.chapter13

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {

  def mkString(arr: Array[Any]): String = {
    arr.map(_.toString).reduceLeft(_+_)
  }

  println(mkString(Array(1, 2, 4, 5, 6)))
}
