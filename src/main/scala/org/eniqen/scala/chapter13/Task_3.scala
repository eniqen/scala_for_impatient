package org.eniqen.scala.chapter13

import scala.collection.mutable.LinkedList


/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {

  val list = List(0, 3, 1, 0, 4, 7, 2, 4, 0, 3)

  def filtered(list: List[Int]): List[Int] = list match {
    case Nil => Nil
    case h :: t => if (h == 0) filtered(t) else h :: filtered(t)
  }

  def defaultFiltered(list: List[Int]): List[Int] = {
    for (elem <- list if elem != 0) yield elem
  }

  val linked = LinkedList(0, 3, 1, 0, 4, 7, 2, 4, 0, 3)

  def filteredWithWhile(list: LinkedList[Int]): LinkedList[Int] = {
    var current = linked
    while (current.next != Nil) {
      if (current.next.elem == 0) {
        current.next = current.next.next
      } else current = current.next
    }
    if (list.elem == 0) list.tail else list
  }

  linked.filter(_ != 0)
  println(linked)
//  println(filteredWithWhile(linked))


  //  println(defaultFiltered(list))
  //  println(filtered(list))
}

