package org.eniqen.scala.chapter13


/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  import scala.collection.mutable.ArrayBuffer
  import scala.collection.mutable.Map

  def indexes(s: String): Map[Char, ArrayBuffer[Int]] = {
    val result = Map[Char, ArrayBuffer[Int]]()
    s.zipWithIndex
      .foreach((pair: (Char, Int)) => {
        val orElse = result.getOrElse(pair._1, new ArrayBuffer[Int])
        orElse += pair._2
        result(pair._1) = orElse
      })
    result
  }

  println(indexes("Mississippi"))
}
