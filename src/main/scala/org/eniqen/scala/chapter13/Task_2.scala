package org.eniqen.scala.chapter13

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  def indexes(s: String) = {
    s.zipWithIndex.groupBy(_._1).map(pair => (pair._1, pair._2.map(_._2)))
  }

  println(indexes("Mississippi"))
}
