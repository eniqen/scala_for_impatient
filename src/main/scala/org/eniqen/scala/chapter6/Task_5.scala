package org.eniqen.scala.chapter6

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_5 extends App {
  if (args.length > 0) print(args.reverse.mkString(" "))
}
