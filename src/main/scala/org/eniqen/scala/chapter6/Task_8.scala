package org.eniqen.scala.chapter6

import java.lang.Integer._

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_8 extends App {

  object RGBCube extends Enumeration {
    val RED = Value(toHexString(java.awt.Color.RED.getRGB))
    val GREEN = Value(toHexString(java.awt.Color.GREEN.getRGB))
    val BLUE = Value(toHexString(java.awt.Color.BLUE.getRGB))
    val MAGENTA = Value(toHexString(java.awt.Color.MAGENTA.getRGB))
    val YELLOW = Value(toHexString(java.awt.Color.YELLOW.getRGB))
    val BLACK = Value(toHexString(java.awt.Color.BLACK.getRGB))
    val WHITE = Value(toHexString(java.awt.Color.WHITE.getRGB))
    val ORANGE = Value(toHexString(java.awt.Color.ORANGE.getRGB))
  }

  for (color <- RGBCube.values) println(color)

}
