package org.eniqen.scala.chapter6

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {

  object Conversion {
    def inchesToCentimeters(value: Double) = {
      value * 2.540
    }

    def gallonsToLiters(value: Double) = {
      value * 4.546
    }

    def milesToKilometers(value: Double) = {
      value * 1.609
    }
  }

}
