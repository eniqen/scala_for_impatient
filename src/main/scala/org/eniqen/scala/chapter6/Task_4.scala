package org.eniqen.scala.chapter6

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_4 extends App {

  class Point private(val x: Int, val y: Int)

  object Point {
    def apply(x: Int, y: Int): Point = new Point(x, y)
  }

  println(Point(1, 2))
}
