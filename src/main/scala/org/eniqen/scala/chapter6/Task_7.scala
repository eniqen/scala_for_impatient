package org.eniqen.scala.chapter6

import org.eniqen.scala.chapter6.Task_6.CardSuite._

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {
  def isRed(cardSuite: CardSuite): Boolean ={
    cardSuite == HEART || cardSuite == TILE
  }

  println(isRed(TILE))

}
