package org.eniqen.scala.chapter6

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  object CardSuite extends Enumeration {
    type CardSuite = Value
    val HEART = Value("♥")
    val TILE = Value("♦")
    val CLOVER = Value("♣")
    val PIKE = Value("♠")
  }

  println(CardSuite.CLOVER.toString)
  println(CardSuite.HEART.toString)
  println(CardSuite.TILE.toString)
  println(CardSuite.PIKE.toString)

}
