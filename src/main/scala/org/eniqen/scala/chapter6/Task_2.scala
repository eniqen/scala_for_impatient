package org.eniqen.scala.chapter6

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_2 extends App {

  abstract class UnitConversations {
    def convert(value: Double): Double
  }

  object InchesToKilometers extends UnitConversations {
    override def convert(value: Double) = value * 2.540
  }

  object GallonsToLiters extends UnitConversations {
    override def convert(value: Double) = value * 4.546
  }

  object MilesToKilometers extends UnitConversations {
    override def convert(value: Double) = value * 1.609
  }

}
