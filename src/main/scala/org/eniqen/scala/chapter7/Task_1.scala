package org.eniqen.scala.chapter7

import org.eniqen.scala.chapter7.com.horstman.impatient.TestClass

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_1 extends App {
  new TestClass
}

package com.horstman.impatient {

  object Test {
    val test = "Test"
  }

}

package com {
  package horstman {
    package impatient {
      class TestClass {
        println(Test.test)
      }
    }
  }
}