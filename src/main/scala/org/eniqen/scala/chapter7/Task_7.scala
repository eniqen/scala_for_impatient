package org.eniqen.scala.chapter7

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_7 extends App {

  import java.util.{HashMap => JavaMap}
  import scala.collection.mutable.{HashMap => ScalaMap}

  val javaMap = new JavaMap[Int, String]()
  javaMap.put(1, "A")
  javaMap.put(2, "B")
  javaMap.put(3, "C")

  val scalaMap = new ScalaMap[Int, String]()

  javaMap.forEach((k, v) => scalaMap.put(k, v))

  println(scalaMap)
}
