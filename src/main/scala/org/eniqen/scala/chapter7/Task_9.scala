package org.eniqen.scala.chapter7
import java.lang.System._


/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_9 extends App {

  private val username = getProperty("user.key")
  private val password = readLine()

  if("secret".equals(password)) print("correct")
  else err.print("incorrect password")
}
