package org.eniqen.scala.chapter7
import java.util.{HashMap => JavaMap}
import scala.collection.mutable.{HashMap => ScalaMap}

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_6 extends App {

  val javaMap = new JavaMap[Int, String]()
  javaMap.put(1, "A")
  javaMap.put(2, "B")
  javaMap.put(3, "C")

  val scalaMap = new ScalaMap[Int, String]()

  javaMap.forEach((k, v) => scalaMap.put(k, v))

  println(scalaMap)
}
