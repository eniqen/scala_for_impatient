package org.eniqen.scala.chapter7

/**
  * @author Mikhail Nemenko { @literal <nemenkoma@gmail.com>}
  */
object Task_3 extends App {
  println(random.nextInt)
  println(random.nextDouble)
}

package object random {
  var previous = 0
  val a = 1664525
  val b = 1013904223
  val n = 32

  def nextInt(): Int = {
      (previous * a + b) % (2 * n)
  }

  def nextDouble(): Double = {
    nextInt.toDouble
  }

  def setSeed(seed: Int): Unit ={
    previous = seed
  }
}
